package com.example.hapychat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MainActivity extends AppCompatActivity {

    Button btnregister,btnlogin;
    TextView forgotPass;
    TextInputEditText email1,pass1;
    String email,pass;
    private FirebaseAuth auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        FirebaseApp.initializeApp(this);
        auth = FirebaseAuth.getInstance();

        btnregister= findViewById(R.id.btnregister);
        btnlogin = findViewById(R.id.btnlogin);
        forgotPass= findViewById(R.id.forgotpass);
        email1= findViewById(R.id.email);
        pass1= findViewById(R.id.pass1);



        btnregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,CreateAccout.class);
                startActivity(intent);
            }
        });

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginUser();
            }
        });

        forgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,ForgotPass.class);
                startActivity(intent);
            }
        });

    }
    //validations
    private void loginUser() {
        email = email1.getText().toString().trim();
        pass =pass1.getText().toString().trim();

        if (!TextUtils.isEmpty(email)){
            loginUserFirebase(email,pass);

        }  else {
            Toast.makeText(this, "Email and Password fields have an issue", Toast.LENGTH_SHORT).show();
            SweetAlertDialog errorDialog = new SweetAlertDialog(MainActivity.this,SweetAlertDialog.ERROR_TYPE);
            errorDialog.setTitle("Email and Password Fields not correct");
            errorDialog.setCancelable(true);
            errorDialog.setCanceledOnTouchOutside(false);
            errorDialog.show();
        }


        if (!TextUtils.isEmpty(pass)){
            loginUserFirebase(email,pass);

        }  else {

            Toast.makeText(this, "Email and Password fields have an issue", Toast.LENGTH_SHORT).show();
            SweetAlertDialog errorDialog = new SweetAlertDialog(MainActivity.this,SweetAlertDialog.ERROR_TYPE);
            errorDialog.setTitle("Email and Password Fields not correct");
            errorDialog.setCancelable(true);
            errorDialog.setCanceledOnTouchOutside(false);
            errorDialog.show();
        }

    }

    private void loginUserFirebase(String email, String pass) {
        //giving user progress
        SweetAlertDialog progressDialog = new SweetAlertDialog(MainActivity.this,SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.setTitleText("Signing in.. Please wait");
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        auth.signInWithEmailAndPassword(email,pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    progressDialog.dismiss();
                    SweetAlertDialog successDialog = new SweetAlertDialog(MainActivity.this,SweetAlertDialog.SUCCESS_TYPE);
                    successDialog.setTitleText("Account Verified" + task.isSuccessful());
                    successDialog.setCancelable(true);
                    successDialog.show();
                    updateUi();
                } else if (!task.isSuccessful()){
                    progressDialog.dismiss();
                    SweetAlertDialog errorDialog = new SweetAlertDialog(MainActivity.this,SweetAlertDialog.ERROR_TYPE);
                    errorDialog.setTitleText("Error in signing in");
                    errorDialog.setCancelable(true);
                    errorDialog.setCanceledOnTouchOutside(false);
                    errorDialog.show();
                }
            }
        });
    }

    private void updateUi() {
        Intent intent = new Intent(MainActivity.this,ChatMessageActivity.class);
        startActivity(intent);
    }
}