package com.example.hapychat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class ForgotPass extends AppCompatActivity {
        TextView login;
        TextInputEditText email;
        private FirebaseAuth auth;
        String emailForgot;
        Button btnresetpass,btnlogin;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_forgot_pass);

            FirebaseApp.initializeApp(this);
            auth  = FirebaseAuth.getInstance();
            btnresetpass= findViewById(R.id.btnresetpass);
            email= findViewById(R.id.email);
            btnlogin = findViewById(R.id.btnlogin);
            btnlogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ForgotPass.this,MainActivity.class);
                    startActivity(intent);
                }
            });

            btnresetpass.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    forgotPassword();
                }
            });
        }

        private void forgotPassword() {
            emailForgot = email.getText().toString().trim();
            if (TextUtils.isEmpty(emailForgot)){
                Toast.makeText(this, "Email and Password fields have an issue", Toast.LENGTH_SHORT).show();
                SweetAlertDialog errorDialog = new SweetAlertDialog(ForgotPass.this,SweetAlertDialog.ERROR_TYPE);
                errorDialog.setTitle("Email cannot be empty");
                errorDialog.setCancelable(true);
                errorDialog.setCanceledOnTouchOutside(false);
                errorDialog.show();
            } else {
                resetPassFirebase(emailForgot);
            }
        }

        private void resetPassFirebase(String emailForgot) {
            //giving user progress
            SweetAlertDialog progressDialog = new SweetAlertDialog(ForgotPass.this,SweetAlertDialog.PROGRESS_TYPE);
            progressDialog.setTitleText("Sending reset link... Please wait");
            progressDialog.setCancelable(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();

            auth.sendPasswordResetEmail(emailForgot).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()){
                        progressDialog.dismiss();
                        SweetAlertDialog successDialog = new SweetAlertDialog(ForgotPass.this,SweetAlertDialog.SUCCESS_TYPE);
                        successDialog.setTitleText("Email link sent" + task.isSuccessful());
                        successDialog.setCancelable(true);
                        successDialog.show();
                        updateUi();
                    } else if(!task.isSuccessful()){
                        progressDialog.dismiss();
                        SweetAlertDialog errorDialog = new SweetAlertDialog(ForgotPass.this,SweetAlertDialog.ERROR_TYPE);
                        errorDialog.setTitleText("Error in resetting the password");
                        errorDialog.setCancelable(true);
                        errorDialog.setCanceledOnTouchOutside(false);
                        errorDialog.show();
                    }

                }
            });

        }

        private void updateUi() {
            Intent intent = new Intent(ForgotPass.this,MainActivity.class);
            startActivity(intent);
        }
    }