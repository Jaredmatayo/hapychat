package com.example.hapychat.model;

public class ChatModel {
    //declare variables
    String userMessage;
    String userImage;

    //costructor
    public ChatModel(){

    }

    //matching constructor
    public ChatModel(String userMessage,String userImage) {
        this.userMessage = userMessage;
        this.userImage = userMessage;

    }
//GENERATE

    public String getUserMessage() {
        return userMessage;
    }

    public void setUserMessage(String userMessage) {
        this.userMessage = userMessage;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

}
