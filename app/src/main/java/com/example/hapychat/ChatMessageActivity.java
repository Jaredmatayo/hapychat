package com.example.hapychat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hapychat.companies.Projects;
import com.example.hapychat.model.ChatMessage;
import com.example.hapychat.socialAcounts.Jobs;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.database.FirebaseListAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

public class ChatMessageActivity extends AppCompatActivity {

    EditText editmessage;
    FloatingActionButton send;

    private DrawerLayout drawerLayout;
    Toolbar toolbar;

    private ActionBarDrawerToggle toggle;
    private NavigationView nv;

    private static final int SIGN_IN_REQUEST_CODE =1 ;
    private FirebaseListAdapter<ChatMessage> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_message);

        //find views by ids
        drawerLayout=findViewById(R.id.chatplatform);
        toolbar=findViewById(R.id.toolbar);


        if(FirebaseAuth.getInstance().getCurrentUser() !=null) {

            // a welcome Toast
            Toast.makeText(this, "Welcome " + FirebaseAuth.getInstance()
                            .getCurrentUser()
                            .getDisplayName(), Toast.LENGTH_LONG).show();

            // Load chat messages
            displayChatMessages();

        } else {
            //direct user to sign in activity
            Intent intentlogin=new Intent(ChatMessageActivity.this,MainActivity.class);
            startActivity(intentlogin);
        }

        send=findViewById(R.id.fab);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editmessage=findViewById(R.id.input);

                // Read the input field and push a new instance
                // of ChatMessage to the Firebase database
                FirebaseDatabase.getInstance()
                        .getReference()
                        .push()
                        .setValue(new ChatMessage(editmessage.getText().toString(),
                                FirebaseAuth.getInstance()
                                        .getCurrentUser()
                                        .getDisplayName())
                        );

                // Clear the input
                editmessage.setText("");
            }
        });

        //navigation drawer
        setSupportActionBar(toolbar);


        toggle= new ActionBarDrawerToggle(this, drawerLayout,R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        nv = findViewById(R.id.nv);
        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                //items from the menu using ids
                // Handle navigation view item clicks here.
                int id = item.getItemId();

                if (id == R.id.account) {
                    // Handle the camera action
                }  else if (id == R.id.logout) {
                    finishAffinity();
                    Intent intentL=new Intent(ChatMessageActivity.this,MainActivity.class);
                    startActivity(intentL);

                } else if (id == R.id.project) {
                    Intent intentprojects=new Intent(ChatMessageActivity.this, Projects.class);
                    startActivity(intentprojects);

                }
                else if (id == R.id.jobs) {
                    Intent intentjobs=new Intent(ChatMessageActivity.this, Jobs.class);
                    startActivity(intentjobs);

                }

                return true;

            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(toggle.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    private void displayChatMessages() {

        ListView listOfMessages = findViewById(R.id.list_of_messages);

        adapter =new FirebaseListAdapter<ChatMessage>(this,ChatMessage.class,
                R.layout.message, FirebaseDatabase.getInstance().

                getReference())

        {
            @Override
            protected void populateView (View v, ChatMessage model, int position){
                // Get references to the views of message.xml
                TextView messageText = v.findViewById(R.id.message_text);
                TextView messageUser = v.findViewById(R.id.message_user);
                TextView messageTime = v.findViewById(R.id.message_time);

                // Set their text
                messageText.setText(model.getMessageText());
                messageUser.setText(model.getMessageUser());

                // Format the date before showing it
                messageTime.setText(DateFormat.format("dd-MM-yyyy (HH:mm:ss)",
                        model.getMessageTime()));
            }
        }

        ;

        listOfMessages.setAdapter(adapter);

    }

}