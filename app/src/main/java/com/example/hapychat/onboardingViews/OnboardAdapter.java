package com.example.hapychat.onboardingViews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.example.hapychat.R;

import java.util.ArrayList;

public class OnboardAdapter extends PagerAdapter {

    //context
    private final Context context;
    ArrayList<OnboardModel> onboardModels=new ArrayList<>();

    //constructor
    public OnboardAdapter(Context context,ArrayList<OnboardModel> onboardModels){
        this.context = context;
        this.onboardModels = onboardModels;
    }



    @Override
    public int getCount() {
        return onboardModels.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        //inflating layout
        View v = LayoutInflater.from(context).inflate(R.layout.onboard_item,container,false);
        //determine position of items
        OnboardModel onboardingModel = onboardModels.get(position);
        //referencing our views
        ImageView imageView = v.findViewById(R.id.iv_onboard);
        //set resources
        imageView.setImageResource(onboardingModel.getImageId());
        TextView textViewTitle = v.findViewById(R.id.tv_header);
        textViewTitle.setText(onboardingModel.getTitle());
        TextView textViewDesc = v.findViewById(R.id.tv_desc);
        textViewDesc.setText(onboardingModel.getDescription());

        //add views to container
        container.addView(v);
        //return
        return v;
    }

    //destroy item

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout) object);
    }
}

