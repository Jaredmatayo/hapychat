package com.example.hapychat;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class CreateAccout extends AppCompatActivity {
    Button btncreate,btnlogin;
    TextInputEditText username,email1,createpass1,conpass2;
    String UserName,email,password,conpass12;
    //firebase
    private FirebaseAuth auth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_accout);

        //intialize firebase app
        FirebaseApp.initializeApp(this);
        //intialize the firebase auth service
        auth = FirebaseAuth.getInstance();

        btnlogin = findViewById(R.id.btnlogin);
        btncreate = findViewById(R.id.btncreate);
        username= findViewById(R.id.username);
        email1 = findViewById(R.id.useremail);
        createpass1 = findViewById(R.id.createpass1);
        conpass2= findViewById(R.id.conpass2);

        btncreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createAccount();
            }
        });
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CreateAccout.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }


    //validation
    private void createAccount() {
        UserName=username.getText().toString().trim();
        email = email1.getText().toString().trim();
        password = createpass1.getText().toString().trim();
        conpass12= conpass2.getText().toString().trim();
        //validation
        if (!TextUtils.isEmpty(email)) {
            //create account in firebase
            createAccountFirebase(UserName,email,password);
        } else {
            SweetAlertDialog errorDialog = new SweetAlertDialog(CreateAccout.this,SweetAlertDialog.ERROR_TYPE);
            errorDialog.setTitle("Email and Password Fields not correct");
            errorDialog.setCancelable(true);
            errorDialog.setCanceledOnTouchOutside(false);
            errorDialog.show();

        }

        if (TextUtils.isEmpty(password)) {
            //create account in firebase
            createAccountFirebase(UserName,email,password);
        } else {

            SweetAlertDialog errorDialog = new SweetAlertDialog(CreateAccout.this,SweetAlertDialog.ERROR_TYPE);
            errorDialog.setTitle("Email and Password Fields not correct");
            errorDialog.setCancelable(true);
            errorDialog.setCanceledOnTouchOutside(false);
            errorDialog.show();
        }

        if (password == conpass12) {
            Toast.makeText(this, "Email and Password fields have an issue", Toast.LENGTH_SHORT).show();
            SweetAlertDialog errorDialog = new SweetAlertDialog(CreateAccout.this,SweetAlertDialog.ERROR_TYPE);
            errorDialog.setTitle("Email and Password Fields not correct");
            errorDialog.setCancelable(true);
            errorDialog.setCanceledOnTouchOutside(false);
            errorDialog.show();


        } else {
            //create account in firebase
            createAccountFirebase(UserName,email,password);
        }
    }
    //creating account
    private void createAccountFirebase(String UserName,String email, String password) {
        //giving user progress
        SweetAlertDialog progressDialog = new SweetAlertDialog(CreateAccout.this,SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.setTitleText("Creating Account.. Please wait");
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        //method to create acc in firebase is : CreateAccountUsingEmailandPassword
        auth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    progressDialog.dismiss();
                    SweetAlertDialog successDialog = new SweetAlertDialog(CreateAccout.this,SweetAlertDialog.SUCCESS_TYPE);
                    successDialog.setTitleText("User created successfully" + task.isSuccessful());
                    successDialog.setCancelable(true);
                    successDialog.show();
                    updateUi();
                } else if (!task.isSuccessful()){
                    progressDialog.dismiss();
                    SweetAlertDialog errorDialog = new SweetAlertDialog(CreateAccout.this,SweetAlertDialog.ERROR_TYPE);
                    errorDialog.setTitleText("Error in creating account");
                    errorDialog.setCancelable(true);
                    errorDialog.setCanceledOnTouchOutside(false);
                    errorDialog.show();
                }
            }
        });

    }

    private void updateUi() {
        Intent intent = new Intent(CreateAccout.this,MainActivity.class);
        startActivity(intent);
    }
}